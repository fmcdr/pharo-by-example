# 第1章 关于本书

此版本基于本书以前的版本由 Andrew P. Black, Stéphane Ducasse, Oscar Nierstrasz, Damien Pollet, Damien Cassou 和 Marcus Denker 所写的，Pharo by Example。它还建立在为Pharo 50发布的版本，更新的Pharo by Example，由Stéphane Ducasse撰写，由Dmitri Zagidulin, Nicolai Hess和Dimitris Chloupis编辑。

自上一版以来，Pharo的工具在许多方面都发生了变化，我们努力介绍了这些变化:

* 我们把这本书的开头重新组织起来，以使各章更短。
* 我们用一个简单的反例介绍了一个新的“第一次接触”章节。这可以让读者看到定义类的最重要操作、类的测试和保存代码。
* 我们简要介绍了新的系统浏览器Calypso以及Pharo Launcher。
* 我们引入了一个新的章节，涵盖了 Iceberg 和包管理。关于Pharo和git的更大的书可以在 http://books.pharo.org 上找到。
* 我们增加了新的 Traits 章节。
* 我们简化了SUnit章节，因为现在可以在 http://books.pharo.org/ 上找到一本配套的书
* 我们修改了之前版本中错误的部分内容，比如 Morphic 章节;当前版本比 Pharo by Example 5 有了相当大的改进。

Pharo by Example 已经收到了许多编辑，修改和更新，使其与当前版本的Pharo一致。它不会神奇地自我更新;进步是许多人努力工作的结果;人们不能简单地把‘5.0’改成‘9’。这就是为什么我决定，作为这本书的原作者和主要作者之一，邀请Sebastijan, Gordana和Quentin作为《Pharo by Example 9》的作者。

## 1\.1 Pharo是什么？

Pharo是一种现代的、开源的、动态类型语言，它支持实时编码，并受到Smalltalk的启发。Pharo及其生态系统由六个基本元素组成:

* 一种具有极简语法的动态类型语言，类似于自然语言写作，整个语法可以放在一张明信片上，不熟悉它的人也可以阅读。
* 一个实时编码环境，允许程序员在代码执行时无缝地修改代码。
* 一个强大的IDE，提供工具来帮助管理复杂的代码并促进良好的设计。
* 一个丰富的库，它创建了一个非常强大的环境，可以被视为一个虚拟的操作系统，包括一个非常快速的JIT虚拟机，以及通过FFI提供对操作系统的库和特性的完全访问。
* 一种鼓励并高度重视改变和改进的文化。
* 一个欢迎来自世界任何角落、任何技能水平或经验、使用任何编程语言的程序员的社区。

Pharo致力于为专业软件开发提供一个精简的、开放的平台，同时也为动态语言和环境的研究和开发提供一个健壮的、稳定的平台。Pharo作为Seaside web开发框架的参考实现，可在 http://www.seaside.st 上获得。

Pharo的核心只包含根据MIT许可贡献的代码。Pharo项目始于2008年3月，是Squeak(SmallTalk-80的现代实现)的一个分支，第一个1.0测试版于2009年7月31日发布。从那时起，Pharo每年或一年半都会推出新版本。目前的版本是Pharo 9.0，于2021年7月发布。

Pharo是高度可移植的。Pharo可以在OS X、Windows、Linux、Android、iOS和Raspberry PI上运行。它的虚拟机是完全用Pharo的一个子集编写的，因此很容易在Pharo内部进行模拟、调试、分析和更改。Pharo是一系列创新项目的载体，从多媒体应用程序和教育平台到商业网络开发环境。

Pharo背后有一个重要的原则：Pharo不仅仅是复制过去，它还重塑了Smalltalk的本质。然而，我们意识到，从头开始的大爆炸风格的方法很少取得成功。相反，Pharo支持渐进式的改进。与其一步跨跃到完美的解决方案，大量的小改进既保持了前沿的相对稳定，同时还允许我们试验重要的新特性和库。这促进了社区的贡献和快速反馈，这是Pharo取得成功的基础。最后，Pharo不是一成不变的：来自社区的更改每天都会集成在一起。Pharo大约有100名贡献者，总部设在世界各地。你也可以对Pharo产生影响！只要看看 http://github.com/pharo-project/pharo .就知道了

## 1.2 谁适合阅读本书

这本书不会教你如何编程。读者应该对编程语言有一定的了解。具备一些面向对象编程的背景也会很有帮助。

本书将介绍Pharo编程环境、语言和相关工具。您将接触到常见的习惯性用法和实践，但重点是技术，而不是面向对象的设计。我们将尽可能多地向您展示好的、说明性的例子。

**Pharo幕课**

为Pharo开设的一门优秀的大型在线公开课，在 http://mooc.pharo.org 可以免费获取。幕课很好地介绍了Pharo和面向对象编程，是本书很好的补充。

**延伸阅读**

这本书不会教给你你所需要或你想要了解的关于Pharo的一切。以下是您可以在 http://books.pharo.org 上找到的其他书籍的简短评论列表：

* *Learning Object-Oriented Programming, Design and TDD with Pharo*. 这本书教授面向对象设计和测试驱动开发的关键方面。这是一本学习面向对象编程的好书。
* *Pharo with Style* 这是一本必读的书。它讨论了如何编写良好且可读的Pharo代码。在短短一个小时内，您将提升代码的标准。
* *The Spec UI framework* 本书将向您展示如何用Pharo开发标准用户界面应用程序。

更多的技术书籍还有：

* *Managing your code with Iceberg* 这本书更详细地介绍了如何使用Git管理代码
* *Enterprise Pharo* 这本书包含了与网络、转换器、报告和文档相关的不同章节，这些都是交付应用程序所需要的。
* *Deep into Pharo* 这本书通过实例涵盖了比《Pharo by Example》 更高级的主题。

还有一些书可以开阔你的视野：

* *A simple reflective object kernel* 将带你踏上构建一个反射式语言核心的小旅程，重温“对象一路向下”的所有基本点。它真的很棒。

此外，在 http://stephane.ducasse.free.fr/FreeBooks.html .上还有许多关于SmallTalk的其他免费书籍

## 1.3 忠告

不要因为你没有立刻理解Pharo的某些部分感到沮丧：你不需要知道所有的事情。艾伦·奈特对此的表述如下：

> **试着不去关心**。刚入门的Pharo程序员经常遇到麻烦，因为他们认为在使用一个东西之前，需要先了解它如何工作的所有细节。这意味着他们需要很长一段时间才能掌握`Transcript show: 'Hello World'`。OO[面向对象编程]的一大飞跃就是能够用“我不在乎”去回答“这是如何工作的”这个问题。

如果您不了解简单或复杂的内容，请毫不犹豫地在我们的邮件列表(Pharo-Users@lists.pharo.org或pharo-dev@lists.pharo.org)、IRC和Discorde上询问我们。我们喜欢提问，欢迎任何技能的人。

## 1.4 一本开放的书

在下面的意义上，本书是一本开放的书：

* 本书的内容是在知识共享署名-共享类似(by-sa)许可下发布的。简而言之，您可以自由地分享和改编这本书，只要您遵守以下URL http://creativecommons.org/licenses/by-sa/3.0/ 上提供的许可条件。
* 这本书只描述了Pharo的核心。我们鼓励其他人贡献我们没有描述的Pharo部分的章节。如果您想参与这项工作，请与我们联系。我们想看到更多关于Pharo的书！
* 也可以通过GitHub直接为这本书投稿。只需遵循那里的说明，并询问您在邮件列表，IRC或Discord上的任何问题。您可以在 https://github.com/SquareBracketAssociates/PharoByExample90  上找到GitHub repo。

## 1.5 Pharo社区

Pharo社区是友好和活跃的。以下是您可能会发现有用的资源的简短列表：

* http://www.pharo.org 是Pharo的官方网站。
* http://www.github.com/pharo-project/pharo 是Pharo的GitHub主账户。
* Pharo有一个活跃的Discord服务器-一个基于irc的聊天平台，只需在Pharo网站 http://pharo.org/community ，的Discord部分请求邀请。欢迎大家光临。
* Pharoer们在Pharo上建立了一个wiki: https://github.com/pharo-open-documentation/
  pharo-wiki
* 与项目一起维护的是一个很棒的目录： https://github.com/phro-Open-Documentation/AWEW-pharo
* 如果你听说过SmalltalkHub，http://www.smalltalkhub.com/ 在大约10年的时间里相当于Pharo项目的SourceForge/GitHub。Pharo的许多额外的包和项目都住在那里。现在社区主要是使用GitHub、GitLab和BitBucket等Git存储库。

## 1.6 示例和练习

我们试图提供尽可能多的例子。具体地说，有一些示例显示了可以计算的代码片段。当您选择一个表达式并从其上下文菜单中选择 **print it** 它时，我们使用一个长箭头来指示您获得的结果：

Listing 1-1 小示例

```
3 + 4
>>> 7 "如果你选中 3 + 4 然后 'print it'，你会看到 7"
```

如果您想在Pharo中使用这些代码片段，可以从原著网站的参考资料侧栏下载一个纯文本文件，其中包含所有示例代码：http://books.pharo.org/pharo-by-example

## 1.7 排版惯例

我们总是以方法的类作为方法源代码的前缀。这样，您就可以知道该方法是哪个类。

例如，该书将一种方法显示为：

```
MyExampleSetTest >> testIncludes
    | full empty|
    full := Set with: 5 with: 6.
    empty := Set new.
    self assert: (full includes: 5).
    self assert: (full includes: 6).
    self assert: (empty includes: 5) not
```

如果你想把它输入到Pharo中，你应该在对应的类中输入以下内容。

```
testIncludes
    | full empty |
    full := Set with: 5 with: 6.
    empty := Set new.
    self assert: (full includes: 5).
    self assert: (full includes: 6).
    self assert: (empty includes: 5) not
```

## 1.8 致谢

我们要感谢Alan Kay、Dan Ingalls和他们的团队，他们制作了Squeak，一个令人惊叹的Smalltalk开发环境，成为了Pharo所扎根的开源项目。如果没有Squeak开发人员令人难以置信的工作，Pharo也不可能出现。

我们也要感谢Hilaire Fernandes和Serge Stinckwich允许我们翻译他们在Smalltalk上的部分专栏，以及Damien Cassou贡献了关于“流”的章节。我们特别感谢亚历山大·贝尔热尔、奥拉·格里维、法布里齐奥·佩林、卢卡斯·荣格利、豪尔赫·雷西亚和欧文·韦恩利的详细审查。我们感谢瑞士伯尔尼大学慷慨地支持这个开源项目，并为本书的网站提供了多年的托管。

我们也感谢Pharo社区对这本书项目的热情支持，以及对《Pharo by Example》第一版的所有翻译。

## 1.9 特别致谢

我们要感谢这本书的原著作者！如果没有最初的版本，就很难制作这个版本。《Pharo by Example》是Pharo欢迎新人的中心读物，具有巨大的价值。

特别感谢大卫·威克斯大量的拷贝-编辑工作。

感谢禤浩焯·桑帕莱努、曼弗雷德·克洛内特、马库斯·施莱格、沃纳·卡森斯、迈克尔·奥基夫、阿里耶·霍夫曼、保罗·麦金托什、高拉夫·辛格、吉吉亚萨·格罗弗、克雷格·艾伦、塞尔日·斯坦克维奇、AVH-ON1、尤里·蒂姆丘克、齐奥·皮埃特罗、维维安·莫罗、周立伟。特别感谢Damien Cassou和Cyril Ferlicot在新书更新方面的巨大帮助。

最后，我们要感谢Inria提供了稳定且重要的资金支持，以及RMoD团队成员不断推动Pharo前进的能量。我们还想感谢Pharo财团的成员。

非常感谢Damien Pollet的这本伟大的书模板，以及Joseph Guégan让我们使用他伟大的Eckmuhl灯塔楼梯图片的权利。

S. Ducasse, S. Kaplar, Gordana Rakic, and Q. Ducasse
