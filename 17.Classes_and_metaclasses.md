# 第16章 类和元类

在Pharo中，所有东西都是对象，每一个对象都是某个类的实例。类本身也不例外：类本身也是对象，类对象也是某个类的实例。这种模式是精简、简单、优雅和统一的。它完全抓住了面向对象编程的精髓。然而，这种一致性的含义可能会让新手感到困惑。

注意，你不需要完全理解这种一致性的含义，就可以熟练地使用Pharo编程。然而，这一章的目标有两个：（1）尽可能深入；（2）表明这里没有复杂、神奇或特殊的东西：只是简单规则的统一应用。遵循这些规则，你总能理解为什么情况是这样的。

## 类的规则

Pharo对象模型基于少数几个统一应用的概念。为了重温记忆，下面是我们在第1章：Pharo对象模型中探讨的规则。

- 规则1 任何东西都是对象
- 规则2 任何对象都是某个类的实例
- 规则3 每一个类都有一个超类
- 规则4 所有的计算都是通过消息发送来进行的
- 规则5 方法查找遵循继承链
- 规则6 类也是对象，并且遵循同样的规则

规则1的结果是，类也是对象，因此规则2告诉我们，类也必须是某个类的实例。类所属的类称为“元类”。

## 17.2 元类

当你定义一个新类时，都会自动创建一个元类。大多数时候，你并不需要关心元类。然而，当你使用浏览器浏览类的`class side`时，想象一下你实际上是在浏览一个不同的类是很有帮助的。类以及元类是两个独立的类。事实上，`Point`与`Point class`是不同的，对于类及其元类也是如此。

为了正确地解释类及元类，我们需要附加的规则进行扩展：

- 规则7 每一个类都是某个元类的实例
- 规则8 元类的层次结构与类的层次结构平行
- 规则9 每一个元类都继承自`Class`和`Behavior`
- 规则10 每一个元类都是`Metaclass`的实例
- 规则11 `Metaclass`的元类也是`Metaclass`的实例

这11条规则一起构成了Pharo完整的对象模型。

我们将首先通过一个小例子来简要回顾[第5章：Pharo对象模型]()中的第5条规则。然后，我们将使用相同的例子来仔细研究这些新规则。

## 17.3 重温Pharo对象模型

- 规则1. 因为所有东西都是对象，所以在Pharo中有序collection也是一个对象。

```smalltalk
OrderedCollection withAll: #(4 5 6 1 2 3)
>>> an OrderedCollection(4 5 6 1 2 3)
```

- 规则2. 每个对象都是某个类的实例。有序collection是`OrderedCollection`类的实例：

```smalltalk
(OrderedCollection withAll: #(4 5 6 1 2 3)) class
>>> OrderedCollection
```

- 规则3. 每个类都有一个超类。`OrderedCollection`的超类是`SequenceableCollection`，`SequenceableCollection`的超类是`Collection`:

```smalltalk
OrderedCollection superclass
>>> SequenceableCollection

SequenceableCollection superclass
>>> Collection

Collection superclass
>>> Object
```

- 规则4. 所有的事情都是通过发送消息来产生的，所以我们可以推断：`withAll:`是发送给`OrderedCollection`的消息，而`class`是发送给`OrderedCollection`实例的消息，而`superclass`是发送给`OrderedCollection`和`SequenceableCollection`以及`Collection`类的消息。任何情况下，接收器都是一个对象，因为所有东西都是对象，但这些对象中的某一些也是类。

- 规则5. 方法查找遵循继承链，所以当我们将消息`class`发送到`(OrderedCollection withAll: #(4 5 6 1 2 3)) asSortedCollection`的结果时，当在`Object`类中找到相应的方法时，消息被处理，如图17-1所示。

![figure-17-1](images/figure-17-1.png)

## 17.4 每一个类都是某个元类的实例

正如我们在前面17.2节中提到的，元类的实例本身就是类。这是为了确保我们可以精确地引用`Point`类以及`Point class`的类。

### 元类是隐式的

元类是在定义类的时候自动创建的。我们说它们是隐式的，作为程序员，你永远不需要担心它们。为每一个类创建隐式的元类，因此每个元类只有一个实例。

普通类是有名字的，而元类是匿名的。但是，我们总是可以通过它们的实例（类）来引用它们。`SortedCollection`的类为`SortedCollection class`，`Object`的类为`Object class`：

```smalltalk
SortedCollection class
>>> SortedCollection class

Object class
>>> Object class
```

事实上，元类并不是真正的匿名，它们的名字是从它们的单一实例中推导出来的。

```smalltalk
SortedCollection class name
>>> 'SortedCollection class'

Object class name
>>> 'Object class'
```

图17-2显示了每个类是它的元类的一个实例。注意，由于空间限制，图中跳过了`SequenceableCollection`和`Collection`类。没有它们不会改变整体的意义。

![figure-17-2](images/figure-17-2.png)

## 17.5 查询元类

事实上，类也是对象，这使得我们可以很容易地通过发送消息来查询它们。我们一起来看看：

```smalltalk
OrderedCollection subclasses
>>> {SortedCollection . ObjectFinalizerCollection .
     WeakOrderedCollection . OCLiteralList . GLMMultiValue}
     
SortedCollection subclasses
>>> #()

SortedCollection allSuperclasses
>>> an OrderedCollection(OrderedCollection SequenceableCollection
                         Collection Object ProtoObject)
                         
SortedCollection instVarNames
>>> #(#sortBlock)

SortedCollection allInstVarNames
>>> #(#array #firstIndex #lastIndex #sortBlock)

SortedCollection selectors
>>> #(#sortBlock: #add: #groupedBy: #defaultSort:to: #addAll:
      #at:put: #copyEmpty #, #collect: #indexForInserting:
      #insert:before: #reSort #addFirst: #join: #median #flatCollect:
      #sort: #sort:to: #= #sortBlock)
```

## 17.6 元类的层次结构与类的层次结构平行

规则7表明，元类的超类被约束为该元类唯一的实例的超类的元类，不会是别的类。`SortedCollection`的元类继承自`OrderedCollection`的元类(`SortedCollection`的超类)。

```smalltalk
SortedCollection class superclass
>>> OrderedCollection class

SortedCollection superclass class
>>> OrderedCollection class
```

这就是我们所说的元类的层次结构与类的层次结构是平等的意思。图17-3显示了如何在`SortedCollection`层次结构中工作。

![figure-17-3](images/figure-17-3.png)

```smalltalk
SortedCollection class
>>> SortedCollection class

SortedCollection class superclass
>>> OrderedCollection class

SortedCollection class superclass superclass
>>> SequenceableCollection class

SortedCollection class superclass superclass superclass superclass
>>> Object class
```

## 17.7 类和对象之间的一致性

退一步来看，将消息发送给对象和发送给类之间并没有什么区别，这是很有趣的。在这两种情况下，对相应方法的查找都是从接收者的类中开始，然后沿着继承链向上查找。

因此，发送给类的消息遵循元类的继承链。例如，思考一下在`Collection`的类侧实现的`withAll:`方法。当我们将带有`add:`的消息发送给`OrderedCollection`类时，它将以和其他消息一样的方式进行查找。查找先从`OrderedCollection class`中开始（从接收者的类中开始，接收者是`OrderedCollection`），并沿着元类的层次结构向上查找，一直到从`Collection class`中找到（参见图17-4）。它返回`OrderedCollection`的一个新实例。

![figure-17-4](images/figure-17-4.png)

```smalltalk
OrderedCollection withAll: #(4 5 6 1 2 3)
>>> an OrderedCollection (4 5 6 1 2 3)
```

### 只查找一个方法

在Pharo中只有一种统一的方法查找。类也只是对象，其行为与别的对象一样。类之所以能够创建实例，只是因为类恰好可以响应`new`消息，而且`new`方法知道如何创建新实例。

通常，非类对象不能理解这条消息，但是如果你认为有必要，就没有什么可以阻止你给非元类添加`new`方法。

## 17.8 检视对象和类

因为类也是对象，所以我们也可以检视它们。

检视`OrderedCollection withAll: #(4 5 6 1 2 3)`和`OrderedCollection`。

注意，在一种情况下，我正在检视`OrderedCollection`的一个实例，而在另一种情况下，你正在检视`OrderedCollection`类本身。这可能会令人困惑，因为检视器的标题栏命名了被检视对象的类。

通过检视`OrderedCollection`允许你查看`OrderedCollection`类的超类、实例变量、方法字典等，如图17-5所示：

![figure-17-5](images/figure-17-5.png)

## 17.9 每一个元类都继承自`Class`和`Behavior`

每个元类都是类的一种（具有单个实例的类），因此从`Class`继承。反过来，类继承了它的超类，`Class`,`ClassDescription`和`Behavior`。因为Pharo中的所有东西都是对象，所以这些类最终都继承自`Object`。我们可以在图17-6中看到完整的图片

![figure-17-6](images/figure-17-6.png)

### `new`在哪里定义

要理解元类继承自`Class`和`Behavior`这一事实的重要性，搞清楚`new`在哪里定义以及如何查找到它会有所帮助。

当消息`new`被发送给一个类时，它会在它的元类链中查找，并最终在它的超类`Class`, `ClassDescription`和`Behavior`中查找，如图17-7所示

![figure-17-7](images/figure-17-7.png)

当我们向`SortedCollection`类发送消息`new`时，将在元类`SortedCollection class`中查找该消息，并沿着继承链向上查找.记住，它的查找过程与任何别的对象是一样的。

消息`new`是在哪里被定义的？这个问题至关重要。首先，`new`在`Behavior`类中定义，并且可以在它的子类中重新定义，包括我们定义的类的任何元类，这是必要的。

现在，当一个消息`new`被发送给一个类时，它会像往常一样，在这个类的元类中查找，继续沿着超类链向上，直到`Behavior`类，如果它没有在这个过程中被重新定义过的话。

注意，发送`SortedCollection new`的结果是一个`SortedCollection`的实例，而不是`Behavior`的实例，尽管该方法是在`Behavior`类中找到的。

方法`new`总是返回`self`的一个实例，即接收者的类，即是它是在另一个类（超类）中实现的。

```smalltalk
SortedCollection new class
>>> SortedCollection       "并非Behavior"
```

### 一个常见错误

一个常见的错误是在作为接收者的类的超类中查找`new`。创建给定大小的对象的标准方法`new:`也是如此。例如：`Array new: 4`创建一个包含4个元素的数组。你不可能在`Array`或它的超类中找到这个方法。相反，你应该查找`Array class`及其超类，因为这是查找的起点（参见图17-7）

![figure-17-7](images/figure-17-7.png)

方法`new`和`new:`定义在元类中，因为它们是在响应发送给类的消息时执行的。

此外，由于类是一个对象，它也是在`Object`上所定义的方法的的接收者。当我们将消息`class`或`error:`发送给`Point`类时，方法查找将遍历元类链（在`Point class`中查找，在`Object class`中查找....）一直到`Object`。

## 17.10 `Behavior`, `ClassDescription`和`Class`的职责

### Behavior

`Behavior`提供了具有实例的对象所需的最小状态，其中包括超类链接、方法字典和类格式。类格式是一个整数，用于编码指针/非指针的区别、紧凑/非紧凑的区别以及实例的基本大小。`Behavior`继承自`Object`，所以它和它的所有子类都可以表现得象`Object`一样。

`Behavior`了是编译器的基本接口。它提供了创建方法字典、编译方法、创建实例（比如`new`,`basicNew`,`new:`, `basicNew:`）、操作类层次结构（如`superclass:`, `addSubclass:`）、访问方法（如`selector`,`allSelectors`,`compiledMethodAt:`）、访问实例和变量（如`allInstances`, `instVarNames`...）、访问类层次结构（如`superclass`, `subclasses`）以及查询（如`hasMethods`, `includesSelector`, `canUnderstand:`, `inheritsFrom:`, ``isVariable`）。

### ClassDescription

`ClassDescription`是一个抽象类，它提供了两个直接子类`Class`和`Metaclass`所需的功能。`ClassDescription`为`Behavior`提供的基础添加了许多工具：命名实例变量，将方法分类为协议，维护更改集和记录更改，以及归档更改所需要的大多数机制。

### Class

`Class`表示所有类的共同行为。它提供了类名、编译方法、方法存储和实例变量。它提供了类变量名和共享池变量的具体表示（`addClassVarName:`, `addSharedPool:`, `initialize`）。由于一个类是其元类的唯一实例（即，非元类），所有元类最终都继承自`Class`（如图17-9所示）

![figure-17-9](images/figure-17-9.png)

## 17.11 每一个元类都是`Metaclass`的实例

下一个问题是，既然元类也是对象，它们应该是另一个类的实例，但是哪个类呢？如图17-8所示，它们是`Metaclass`的实例。类`Metaclass`的实例是匿名类，每个元类只有一个实例，也就是一个类。

![figure-17-8](images/figure-17-8.png)

`Metaclass`表示常见的元类行为。它提供了创建实例（`subclassOf:`）’创建元类的唯一实例的初始化实例、类变量初始化、元实实例、方法编译和类信息（继承链、实例变量...）的方法。

## 17.12 `Metaclass`的元类是一个`Metaclass`的实例

最后一个要回答的问题是：`Metaclass class`是什么？

答案很简单：它是一个元类，所以它必须是`Metaclass`的一个实例，就像系统中所有别的类的元类一样（参见图17-9）

![figure-17-9](images/figure-17-9.png)

图17-9显示了所有的元类都是`Metaclass`的实例，包括`Metaclass`本身的元类。如果比较图17-8和图17-9,你将看到元类层次结构是如何完美地映射类层次结构的，一直到`Object`类。

下面的例子向我们展示了如何查询类层次结构来证明图17-9是正确的。（实际上，你会看到我们说了一个善意的谎言——`Object class superclass` 是 `ProtoObject class`，而非`Class`）。在Pharo中，我们必须再越过一个超类才能到达`Class`。

```smalltalk
Collection superclass
>>> Object

Collection class superclass
>>> Object class

Object class superclass superclass
>>> Class

Class superclass
>>> ClassDescription

ClassDescription superclass
>>> Behavior

Behavior superclass
>>> Object

"The class of a metaclass is the class Metaclass"
Collection class class
>>> Metaclass

"The class of a metaclass is the class Metaclass"
Object class class
>>> Metaclass

"The class of a metaclass is the class Metaclass"
Behavior class class
>>> Metaclass

"The class of a metaclass is the class Metaclass"
Metaclass class class
>>> Metaclass

"Metaclass is a special kind of class"
Metaclass superclass
>>> ClassDescription
```

## 17.13 本章总结

本章深入研究了Pharo的统一对象模型，并更全面地解释了类是如何组织的。如果你迷路了或感到困惑，你应该始终记住消息传递才是关键：你在接收者的类中查找方法。这适用于任何接收者。如果在接收者的类中找不到该方法，则在其超类中查找。

- 每一个类都是某个元类的实例。元类是隐式的。当你创建一个类时，会自动创建与之相对应的元类。你所创建的类是它唯一的实例。

- 元类层次结构与类层次结构平行。类的方法查找与普通对象的方法查找平行，并遵循元类的超类链。

- 每个元类都继承自`Class`和`Behavior`。每个类都是一个`Class`，因为元类也是`Class`,所以它们也必须从`Class`继承。`Behavior`提供了所有具有实例的对象的公共行为。

- 每个元类都是`Metaclass`的实例，`ClassDescription`提供了类和元类通用的所有内容。

- `Metaclass`的元类是`Metaclass`的实例。关系的实例形成了一个闭合的循环，所以`Metaclass class class`的结果是`Metaclass`.
